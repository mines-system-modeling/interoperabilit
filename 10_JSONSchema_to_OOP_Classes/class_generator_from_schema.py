# Run on Python 3
import python_jsonschema_objects as pjs

# Person.json is the schema file provided to the class generator  python_jsonschema_objects
builder = pjs.ObjectBuilder("Person.json")
ns = builder.build_classes()
Person = ns.Person
# It prints back the class attributes generated
print ("-------------The class generated details-------------")
print(Person.__dict__)
print ("")
emmanuelPerson = Person(firstName="Emmanuel", lastName="Hodonou")
print ("")
print ("-------------The class instanciation and its using-------------")
print("emmanuelPerson.firstName %s" % emmanuelPerson.firstName)
print ("")
print("emmanuelPerson.lastName %s" % emmanuelPerson.lastName)
print ("")

