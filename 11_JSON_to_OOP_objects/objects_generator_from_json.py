# Run on Python 3
import json
class Student(object):
    def __init__(self, name, username, email):
        self.name = name
        self.username = username
        self.email = email


json_value = '''{
  "name": "Emmanuel Hodonou",
  "username": "ehodonou",
  "email":"emmanuel.hodonou@gmail.com"
}'''

print("---------This is the JSON value used---------")
print(json_value)
print("")

j = json.loads(json_value)
print("---------This is the JSON object generated from json value---------")
print(j)
u = Student(**j)
print(u)
print("")
print("---------This is the JSON value converted back from last json object---------")

print(json.dumps(u.__dict__))
print("")


