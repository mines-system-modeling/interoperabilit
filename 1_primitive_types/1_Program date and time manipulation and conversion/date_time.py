from datetime import datetime
from pytz import utc, timezone
import time

USER_TIMEZONE_SENT = 'Europe/Paris'  # we suppose user send a timezone. Change it if yu want by looking <<https://en.wikipedia.org/wiki/List_of_tz_database_time_zones>>
# Get user timezone to a good format
user_tzInfo = timezone(USER_TIMEZONE_SENT)

# Get current date time following UTC format
nowInUTC = datetime.utcnow()
print """\n Current datetime in UTC is:""", nowInUTC

print """\n We set up now a time zone in our code as a constant <<USER_TIMEZONE_SENT>> :""", USER_TIMEZONE_SENT
# Convert our current time to datetime
inter_time = datetime.strptime(nowInUTC.strftime("%Y-%m-%d %H:%M:%S"), '%Y-%m-%d %H:%M:%S')
# Tell the datetime object that it's in UTC time zone since because datetime objects are 'naive' by default
inter_time = inter_time.replace(tzinfo=utc)
user_time = inter_time.astimezone(user_tzInfo)
print("Current datetime of the user converted in his/her timezone:", user_time.strftime("%Y-%m-%d %H:%M:%S"))


d1 = user_time.strftime("%d/%m/%Y")
print(" Current datetime of the user in format dd/mm/YY:", d1)

d2 = user_time.strftime("%d/%m/%Y %H:%M:%S")
print(" Current datetime of the user in dd/mm/YY H:M:S:", d2)

d3 = user_time.strftime("%d/%m/%Y %H:%M")
print(" Current datetime of the user without the second:", d3)

d4 = user_time.isoformat()
print(" Current datetime of the user in ISO:", d4)

d5 = time.mktime(user_time.timetuple())
print(" Current datetime of the user in timestamp:", d5)

d6 = user_time.fromtimestamp(d5).strftime('%d/%m/%Y %H:%M:%S')
print("Conversion of the last timestamp to a human readable text:", d6)


