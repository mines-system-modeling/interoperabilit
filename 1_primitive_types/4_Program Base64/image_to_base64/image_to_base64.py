# coding: utf-8
import base64

def read_file(file_link):
    """Provide file_link and it opens & read file to return image data"""
    image_data = None
    try:
        image = open(file_link, 'rb')
        image_data = image.read()
    except Exception:
        pass
    return image_data

def write_file(file_name, image_64_decode):
    """Provide file data image_64_decode and it will save file with the name file_name """
    try:
        image_result = open(file_name, 'wb')  # create a writable image and write the decoding result
        image_result.write(image_64_decode)
    except Exception:
        return None
    return True


file_in = 'imt.png' # The file that will be converted. The file is in the folder
file_decoded_name = 'imt_decode.png' # The file name to use to save in the folder
print('We put the file imt.png in the source folder.\n')
image_read = read_file(file_in)
if image_read is None:
    print('An error occurred when we tried to read the file imt.png ')
    exit()
image_64_encoded = base64.encodestring(image_read)
print('This is the file imt.png in base64: ' + str(image_64_encoded))
print('Now from the base64, let\'s convert back the base64 got into a file again.\n')
image_decoded_from_64 = base64.decodestring(image_64_encoded)
result_file_generated = write_file(file_decoded_name, image_decoded_from_64)
print('We just converted back from base64 and saved new file (imt_decode.png) in the same folder.')
