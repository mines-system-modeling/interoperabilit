# coding: utf-8
import base64


sentence = 'Professor is a very good professor and i am happy to have him like my lecturer.J\'écris en Français pour montrer qu\'il supporte les caractères spéciaux.'

print('\n Let\'s considering the following human reading sentence: \n%s' % sentence)

d1 = base64.b64encode(sentence)
print(' \n The sentence encoded in base64 is: %s' % d1)
d2 = base64.b64decode(d1)
print(" The sentence decoded from base64 is: %s \n" % d2)



