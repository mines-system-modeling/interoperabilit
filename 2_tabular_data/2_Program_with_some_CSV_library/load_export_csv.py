# coding: utf-8
import pandas as pd

file_in = 'raw_file.csv'  # The file that will be converted. The file is in the folder
row_of_columns = 1  # The row that indicates the columns. Example: For my file it is row 1
csv_delimiter = ','

data_frame = pd.read_csv(file_in, delimiter=csv_delimiter, header=row_of_columns, encoding="ISO-8859-1")
# calling head() method  to get get top 5 data
data_top = data_frame.head(5)
data_last = data_frame.tail(5)
print("The top 5 of data in the file\n %s" % data_top)
print("The last 5 of data in the file\n %s" % data_last)
columns = list(data_frame.columns)

print("The %d columns of data in the file are:\n %s" % (len(columns), columns))

data_description = data_frame.describe()
print("The file data description is:\n %s" % data_description)

file_to_export_name = 'export_data_transformed.csv'
print("We join now the top 5 and last 5 in a file named: <<%s>>\n" % (file_to_export_name))
dat = data_top.append(data_last)
dat.to_csv(file_to_export_name)
print("The data has been now exported in the file :\n %s. Open the file to see data!" % file_to_export_name)
