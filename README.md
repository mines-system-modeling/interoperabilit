# interoperability


## Project Description
```
This project aims at understanding the data interoperability issues that arise from the integration of existing application, or the evolution of systems.
```
The project description is available [here](https://ci.mines-stetienne.fr/cps2/interop/)

## Project Organization
```
You have 11 main folders and some sub-folders in the folders. The main folders are:

1_primitive_types
2_tabular_data
3_json
4_XML
5_YAML
6_JSON-LD
7_JSONSchema
8_JSONPATH
9_XPATH
10_JSONSchema_to_OOP_Classes
11_JSON_to_OOP_objects

```

## Project requirements
```
You should have installed python >=3 to run python files in the project. 
```

## Run project
```
You can can run one the python file by the following

$ python3 <python file> 

```


Professor: Maxime Lefrançois <br/>
Authors: 
Hodonou Hinnougnon Emmanuel


